package com.example.hellowolrd.Pertemuan2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.hellowolrd.Adapter.MahasiswaRecyclerAdapter;
import com.example.hellowolrd.Model.Mahasiswa;
import com.example.hellowolrd.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list_cardview);

        CardView cv = (CardView) findViewById(R.id.cvMahasiswa);
        MahasiswaRecyclerAdapter mahasiswaCardViewAdapter;

        List<Mahasiswa> mahasiswaList = new ArrayList<>();

        //arry data
        Mahasiswa m1 = new Mahasiswa("tatans", "72170134", "09090909");
        Mahasiswa m2 = new Mahasiswa("dio", "72170135", "09090909");
        Mahasiswa m3 = new Mahasiswa("deo", "72170136", "09090909");
        Mahasiswa m4 = new Mahasiswa("jeje", "72170137", "09090909");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
    }
}