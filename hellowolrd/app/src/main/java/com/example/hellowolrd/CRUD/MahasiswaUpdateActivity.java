package com.example.hellowolrd.CRUD;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hellowolrd.Model.DefaultResult;
import com.example.hellowolrd.Network.GetDataService;
import com.example.hellowolrd.Network.RetrofitClientlnstance;
import com.example.hellowolrd.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        final EditText upNimLama = (EditText)findViewById(R.id.upNimLama);
        final EditText upNama = (EditText)findViewById(R.id.upNama);
        final EditText upNim = (EditText)findViewById(R.id.upNim);
        final EditText upAlamat = (EditText)findViewById(R.id.upAlamat);
        final EditText upEmail = (EditText)findViewById(R.id.upEmail);
        Button btnUpdt = (Button) findViewById(R.id.btnUpdt);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        btnUpdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientlnstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del= service.delete_mhs(
                        upNimLama.getText().toString(),
                        "72170134"

                );

                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MahasiswaUpdateActivity.this,"Berhasil Update", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Error!",Toast.LENGTH_LONG).show();
                    }
                });

                Call<DefaultResult> add= service.add_mhs(
                        upNama.getText().toString(),
                        upNim.getText().toString(),
                        upAlamat.getText().toString(),
                        upEmail.getText().toString(),
                        "kosongkan Jeeee",
                        "72170134"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Error",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

}