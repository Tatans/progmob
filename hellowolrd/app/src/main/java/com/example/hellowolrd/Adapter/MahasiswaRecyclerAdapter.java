package com.example.hellowolrd.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hellowolrd.Model.Mahasiswa;
import com.example.hellowolrd.R;

import java.util.ArrayList;
import java.util.List;

public class MahasiswaRecyclerAdapter extends RecyclerView.Adapter<MahasiswaRecyclerAdapter.ViewHoder> {
    private Context context;
    private List<Mahasiswa> mahasiswasList;

    public MahasiswaRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswasList = new ArrayList<>();
    }
    public List<Mahasiswa> getMahasiswasList() {
        return mahasiswasList;
    }

    public void setMahasiswasList(List<Mahasiswa> mahasiswasList) {
        this.mahasiswasList = mahasiswasList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHoder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder holder, int position) {
        Mahasiswa m = mahasiswasList.get(position);

        holder.tvNama.setText(m.getNama());
        holder.tvNim.setText(m.getNim());
        holder.tvNoTelp.setText(m.getNoTelp());
    }


    @Override
    public int getItemCount() {
      return mahasiswasList.size();
    }

    public class ViewHoder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNim, tvNoTelp;

        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNoTelp = itemView.findViewById(R.id.tvNoTelp);
        }
    }
}
