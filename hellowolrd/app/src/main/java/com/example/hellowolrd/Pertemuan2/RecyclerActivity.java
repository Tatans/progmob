package com.example.hellowolrd.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.hellowolrd.Adapter.MahasiswaRecyclerAdapter;
import com.example.hellowolrd.Model.Mahasiswa;
import com.example.hellowolrd.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data jeee
        List<Mahasiswa> mahasiswaList = new ArrayList<>();

        //jeek
        Mahasiswa m1 = new Mahasiswa("tatans","72170134","09090909");
        Mahasiswa m2 = new Mahasiswa("dio","72170135","09090909");
        Mahasiswa m3 = new Mahasiswa("deo","72170136","09090909");
        Mahasiswa m4 = new Mahasiswa("jeje","72170137","09090909");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswasList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);



    }
}